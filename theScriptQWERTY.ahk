﻿#NoEnv
SendMode Input
SetWorkingDir %A_ScriptDir%

<^~::
Send, ∴
return

<^1::
Send, ∀
return

<^2::
Send, ∃
return

<^3::
Send, ¬
return

<^4::
Send, →
return

<^5::
Send, ↔
return

<^6::
Send, ∧
return

<^7::
Send, ⊼
return

<^8::
Send, ∨
return

<^9::
Send, ⊽
return

<^0::
Send, ⊻
return

<^-::
Send, ◊
return

<^+::
Send, □
return

<^q::
Send, ⊤
return

<^w::
Send, ⊥
return

<^e::
Send, ⊢
return

<^r::
Send, ⊬
return

<^t::
Send, ⊨
return

<^y::
Send, ⊭
return

<^u::
Send, ≔
return

<^<+~::
Send, ≈
return

<^<+1::
Send, ≅
return

<^<+2::
Send, ≪
return

<^<+3::
Send, ≫
return

<^<+4::
Send, ≤
return

<^<+5::
Send, ≥
return

<^<+6::
Send, ≦
return

<^<+7::
Send, ≧
return

<^<+8::
Send, ≺
return

<^<+9::
Send, ≻
return

<^<+0::
Send, ◅
return

<^<+-::
Send, ▻
return

<^<++::
Send, ≠
return

<^<+q::
Send, ↦
return

<^<+w::
Send, ⋉
return

<^<+e::
Send, ⋊
return

<^<+r::
Send, ⋈
return

<^<+t::
Send, ∫
return

<^<+y::
Send, ∮
return

<^<+u::
Send, ⟨
return

<^<+i::
Send, ⟩
return

<^<+o::
Send, √
return

<^<+p::
Send, ±
return

<^<+{::
Send, ∝
return

<^<+}::
Send, ∞
return

<^<+\::
Send, ∈
return

<^<+a::
Send, ∉
return

<^<+s::
Send, ∅
return

<^<+d::
Send, ⋃
return

<^<+f::
Send, ⋂
return

<^<+g::
Send, ⊆
return

<^<+h::
Send, ⊂
return

<^<+j::
Send, ⊄
return

<^<+k::
Send, ⊕
return

<^<+l::
Send, ⊙
return

<^<+;::
Send, ∏
return

<^<+'::
Send, ∐
return

<^<+z::
Send, ∤
return

<^<+x::
Send, ∥
return

<^<+c::
Send, ∦
return

<^<+v::
Send, ⋕
return

<^<+b::
Send, ⌊
return

<^<+n::
Send, ⌋
return

<^<+m::
Send, ⌈
return

<^<+,::
Send, ⌉
return

<^<+.::
Send, ₳
return

^><^~::
Send, α
return

^><^1::
Send, β
return

^><^2::
Send, γ
return

^><^3::
Send, δ
return

^><^4::
Send, ε
return

^><^5::
Send, ζ
return

^><^6::
Send, η
return

^><^7::
Send, θ
return

^><^8::
Send, ι
return

^><^9::
Send, κ
return

^><^0::
Send, λ
return

^><^-::
Send, μ
return

^><^+::
Send, ν
return

^><^q::
Send, ξ
return

^><^w::
Send, ο
return

^><^e::
Send, π
return

^><^r::
Send, ρ
return

^><^t::
Send, σ
return

^><^y::
Send, τ
return

^><^u::
Send, υ
return

^><^i::
Send, φ
return

^><^o::
Send, χ
return

^><^p::
Send, ψ
return

^><^{::
Send, ω
return

^><^+>~::
Send, Α
return

^><^+>1::
Send, Β
return

^><^+>2::
Send, Γ
return

^><^+>3::
Send, Δ
return

^><^+>4::
Send, Ε
return

^><^+>5::
Send, Ζ
return

^><^+>6::
Send, Η
return

^><^+>7::
Send, Θ
return

^><^+>8::
Send, Ι
return

^><^+>9::
Send, Κ
return

^><^+>0::
Send, Λ
return

^><^+>-::
Send, Μ
return

^><^+>+::
Send, Ν
return

^><^+>q::
Send, Ξ
return

^><^+>w::
Send, Ο
return

^><^+>e::
Send, Π
return

^><^+>r::
Send, Ρ
return

^><^+>t::
Send, Σ
return

^><^+>y::
Send, Τ
return

^><^+>u::
Send, Υ
return

^><^+>i::
Send, Φ
return

^><^+>o::
Send, Χ
return

^><^+>p::
Send, Ψ
return

^><^+>{::
Send, Ω
return

<^<!~::
Send, 𝕒
return

<^<!1::
Send, 𝕓
return

<^<!2::
Send, 𝕔
return

<^<!3::
Send, 𝕕
return

<^<!4::
Send, 𝕖
return

<^<!5::
Send, 𝕗
return

<^<!6::
Send, 𝕘
return

<^<!7::
Send, 𝕙
return

<^<!8::
Send, 𝕚
return

<^<!9::
Send, 𝕛
return

<^<!0::
Send, 𝕜
return

<^<!-::
Send, 𝕝
return

<^<!+::
Send, 𝕞
return

<^<!q::
Send, 𝕟
return

<^<!w::
Send, 𝕠
return

<^<!e::
Send, 𝕡
return

<^<!r::
Send, 𝕢
return

<^<!t::
Send, 𝕣
return

<^<!y::
Send, 𝕤
return

<^<!u::
Send, 𝕥
return

<^<!i::
Send, 𝕦
return

<^<!o::
Send, 𝕧
return

<^<!p::
Send, 𝕨
return

<^<!{::
Send, 𝕩
return

<^<!}::
Send, 𝕪
return

<^<!\::
Send, 𝕫
return

<^<!+>~::
Send, 𝔸
return

<^<!+>1::
Send, 𝔹
return

<^<!+>2::
Send, ℂ
return

<^<!+>3::
Send, 𝔻
return

<^<!+>4::
Send, 𝔼
return

<^<!+>5::
Send, 𝔽
return

<^<!+>6::
Send, 𝔾
return

<^<!+>7::
Send, ℍ
return

<^<!+>8::
Send, 𝕀
return

<^<!+>9::
Send, 𝕁
return

<^<!+>0::
Send, 𝕂
return

<^<!+>-::
Send, 𝕃
return

<^<!+>+::
Send, 𝕄
return

<^<!+>q::
Send, ℕ
return

<^<!+>w::
Send, 𝕆
return

<^<!+>e::
Send, ℙ
return

<^<!+>r::
Send, ℚ
return

<^<!+>t::
Send, ℝ
return

<^<!+>y::
Send, 𝕊
return

<^<!+>u::
Send, 𝕋
return

<^<!+>i::
Send, 𝕌
return

<^<!+>o::
Send, 𝕍
return

<^<!+>p::
Send, 𝕎
return

<^<!+>{::
Send, 𝕏
return

<^<!+>}::
Send, 𝕐
return

<^<!+>\::
Send, ℤ
return

<^<^>!~::
Send, 𝓪
return

<^<^>!1::
Send, 𝓫
return

<^<^>!2::
Send, 𝓬
return

<^<^>!3::
Send, 𝓭
return

<^<^>!4::
Send, 𝓮
return

<^<^>!5::
Send, 𝓯
return

<^<^>!6::
Send, 𝓰
return

<^<^>!7::
Send, 𝓱
return

<^<^>!8::
Send, 𝓲
return

<^<^>!9::
Send, 𝓳
return

<^<^>!0::
Send, 𝓴
return

<^<^>!-::
Send, 𝓵
return

<^<^>!+::
Send, 𝓶
return

<^<^>!q::
Send, 𝓷
return

<^<^>!w::
Send, 𝓸
return

<^<^>!e::
Send, 𝓹
return

<^<^>!r::
Send, 𝓺
return

<^<^>!t::
Send, 𝓻
return

<^<^>!y::
Send, 𝓼
return

<^<^>!u::
Send, 𝓽
return

<^<^>!i::
Send, 𝓾
return

<^<^>!o::
Send, 𝓿
return

<^<^>!p::
Send, 𝔀
return

<^<^>!{::
Send, 𝔁
return

<^<^>!}::
Send, 𝔂
return

<^<^>!\::
Send, 𝔃
return

<^<^>!+>~::
Send, 𝓐
return

<^<^>!+>1::
Send, 𝓑
return

<^<^>!+>2::
Send, 𝓒
return

<^<^>!+>3::
Send, 𝓓
return

<^<^>!+>4::
Send, 𝓔
return

<^<^>!+>5::
Send, 𝓕
return

<^<^>!+>6::
Send, 𝓖
return

<^<^>!+>7::
Send, 𝓗
return

<^<^>!+>8::
Send, 𝓘
return

<^<^>!+>9::
Send, 𝓙
return

<^<^>!+>0::
Send, 𝓚
return

<^<^>!+>-::
Send, 𝓛
return

<^<^>!+>+::
Send, 𝓜
return

<^<^>!+>q::
Send, 𝓝
return

<^<^>!+>w::
Send, 𝓞
return

<^<^>!+>e::
Send, 𝓟
return

<^<^>!+>r::
Send, 𝓠
return

<^<^>!+>t::
Send, 𝓡
return

<^<^>!+>y::
Send, 𝓢
return

<^<^>!+>u::
Send, 𝓣
return

<^<^>!+>i::
Send, 𝓤
return

<^<^>!+>o::
Send, 𝓥
return

<^<^>!+>p::
Send, 𝓦
return

<^<^>!+>{::
Send, 𝓧
return

<^<^>!+>}::
Send, 𝓨
return

<^<^>!+>\::
Send, 𝓩
return

<^d::
Send, https://discord.gg/dUPFfby
return

; Uncomment the below for screenshots if your computer has Snip And Sketch.

; ^\::
; Send, +#s
; return

; This is just for Apex Legends on my pc, lol.

; ^@::
; Run, "C:\Program Files (x86)\Steam\steamapps\common\Apex Legends\r5apex.exe"
; return