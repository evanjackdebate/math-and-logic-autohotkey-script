# Explanation Of Project

This project adds the following 5 layers of symbols to your keyboard.

Layer                    | To Select Layer                     | Symbols
------------------------ | ----------------------------------- | -------------------------------------
Logic                    | LCtrl                               | ∴∀∃¬⇒⇔∧⊼∨⊽⊻◊□⊤⊥⊢⊬⊨⊭≔
Math                     | LCtrl + LShift                      | ≈≅≪≫≤≥≦≧≺≻◅▻≠↦⋉⋊⋈∫∮⟨⟩√±∝∞∈∉∅⋃⋂⊆⊂⊄⊕⊙∏∐∤∥∦⋕⌊⌋⌈⌉₳
Greek Alphabet           | LCtrl + RCtrl (shift for uppercase) | αβγδεζηθικλμνξοπρστυφχψωΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩ
Blackboard Bold          | LCtrl + LAlt (shift for uppercase)  | 𝕒𝕓𝕔𝕕𝕖𝕗𝕘𝕙𝕚𝕛𝕜𝕝𝕞𝕟𝕠𝕡𝕢𝕣𝕤𝕥𝕦𝕧𝕨𝕩𝕪𝕫𝔸𝔹ℂ𝔻𝔼𝔽𝔾ℍ𝕀𝕁𝕂𝕃𝕄ℕ𝕆ℙℚℝ𝕊𝕋𝕌𝕍𝕎𝕏𝕐ℤ
Mathematical Bold Script | LCtrl + RAlt (shift for uppercase)  | 𝓪𝓫𝓬𝓭𝓮𝓯𝓰𝓱𝓲𝓳𝓴𝓵𝓶𝓷𝓸𝓹𝓺𝓻𝓼𝓽𝓾𝓿𝔀𝔁𝔂𝔃𝓐𝓑𝓒𝓓𝓔𝓕𝓖𝓗𝓘𝓙𝓚𝓛𝓜𝓝𝓞𝓟𝓠𝓡𝓢𝓣𝓤𝓥𝓦𝓧𝓨𝓩

Each layer starts at the top leftmost key (~) and continues rightward until the layer is exhausted.

For a little video about the project, see www.youtube.com/watch?v=y88ORuBA660.

# Explanation Of Files

* README.md is what you're reading right now.

* theLayout.png is an image of the layout.

* theLayout.svg can be used at www.wasdkeyboards.com to have a custom keyboard made or custom keycaps printed. This is not necessary, but it's a little easier to remember where things are (note that on this image "Layer 4" is actually RCtrl).

* theScriptDVORAK.ahk is the script for DVORAK layout.

* theScriptQWERTY.ahk is the script for QWERTY layout.

# Setup Instructions

1. Download AutoHotkey here: www.autohotkey.com.

1. Download either theScriptDVORAK.ahk or theScriptQWERTY.ahk depending what layout you use.

1. Go to wherever you downloaded the file, and double-click it.

1. Optionally, the script can run on startup. I only know how to do this on Windows though (sorrrryyy). Hit Windows + R, enter "shell:startup" and a folder will open, put the script in the folder.