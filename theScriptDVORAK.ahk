﻿#NoEnv
SendMode Input
SetWorkingDir %A_ScriptDir%

<^$::
Send, ∴
return

<^&::
Send, ∀
return

<^[::
Send, ∃
return

<^{::
Send, ¬
return

<^}::
Send, →
return

<^(::
Send, ↔
return

<^=::
Send, ∧
return

<^*::
Send, ⊼
return

<^)::
Send, ∨
return

<^+::
Send, ⊽
return

<^]::
Send, ⊻
return

<^!::
Send, ◊
return

<^#::
Send, □
return

<^;::
Send, ⊤
return

<^,::
Send, ⊥
return

<^.::
Send, ⊢
return

<^p::
Send, ⊬
return

<^y::
Send, ⊨
return

<^f::
Send, ⊭
return

<^g::
Send, ≔
return

<^<+$::
Send, ≈
return

<^<+&::
Send, ≅
return

<^<+[::
Send, ≪
return

<^<+{::
Send, ≫
return

<^<+}::
Send, ≤
return

<^<+(::
Send, ≥
return

<^<+=::
Send, ≦
return

<^<+*::
Send, ≧
return

<^<+)::
Send, ≺
return

<^<++::
Send, ≻
return

<^<+]::
Send, ◅
return

<^<+!::
Send, ▻
return

<^<+#::
Send, ≠
return

<^<+;::
Send, ↦
return

<^<+,::
Send, ⋉
return

<^<+.::
Send, ⋊
return

<^<+p::
Send, ⋈
return

<^<+y::
Send, ∫
return

<^<+f::
Send, ∮
return

<^<+g::
Send, ⟨
return

<^<+c::
Send, ⟩
return

<^<+r::
Send, √
return

<^<+l::
Send, ±
return

<^<+/::
Send, ∝
return

<^<+@::
Send, ∞
return

<^<+a::
Send, ∈
return

<^<+o::
Send, ∉
return

<^<+e::
Send, ∅
return

<^<+u::
Send, ⋃
return

<^<+i::
Send, ⋂
return

<^<+d::
Send, ⊆
return

<^<+h::
Send, ⊂
return

<^<+t::
Send, ⊄
return

<^<+n::
Send, ⊕
return

<^<+s::
Send, ⊙
return

<^<+-::
Send, ∏
return

<^<+\::
Send, ∐
return

<^<+'::
Send, ∤
return

<^<+q::
Send, ∥
return

<^<+j::
Send, ∦
return

<^<+k::
Send, ⋕
return

<^<+x::
Send, ⌊
return

<^<+b::
Send, ⌋
return

<^<+m::
Send, ⌈
return

<^<+w::
Send, ⌉
return

<^<+v::
Send, ₳
return

^><^$::
Send, α
return

^><^&::
Send, β
return

^><^[::
Send, γ
return

^><^{::
Send, δ
return

^><^}::
Send, ε
return

^><^(::
Send, ζ
return

^><^=::
Send, η
return

^><^*::
Send, θ
return

^><^)::
Send, ι
return

^><^+::
Send, κ
return

^><^]::
Send, λ
return

^><^!::
Send, μ
return

^><^#::
Send, ν
return

^><^;::
Send, ξ
return

^><^,::
Send, ο
return

^><^.::
Send, π
return

^><^p::
Send, ρ
return

^><^y::
Send, σ
return

^><^f::
Send, τ
return

^><^g::
Send, υ
return

^><^c::
Send, φ
return

^><^r::
Send, χ
return

^><^l::
Send, ψ
return

^><^/::
Send, ω
return

^><^+>$::
Send, Α
return

^><^+>&::
Send, Β
return

^><^+>[::
Send, Γ
return

^><^+>{::
Send, Δ
return

^><^+>}::
Send, Ε
return

^><^+>(::
Send, Ζ
return

^><^+>=::
Send, Η
return

^><^+>*::
Send, Θ
return

^><^+>)::
Send, Ι
return

^><^+>+::
Send, Κ
return

^><^+>]::
Send, Λ
return

^><^+>!::
Send, Μ
return

^><^+>#::
Send, Ν
return

^><^+>;::
Send, Ξ
return

^><^+>,::
Send, Ο
return

^><^+>.::
Send, Π
return

^><^+>p::
Send, Ρ
return

^><^+>y::
Send, Σ
return

^><^+>f::
Send, Τ
return

^><^+>g::
Send, Υ
return

^><^+>c::
Send, Φ
return

^><^+>r::
Send, Χ
return

^><^+>l::
Send, Ψ
return

^><^+>/::
Send, Ω
return

<^<!$::
Send, 𝕒
return

<^<!&::
Send, 𝕓
return

<^<![::
Send, 𝕔
return

<^<!{::
Send, 𝕕
return

<^<!}::
Send, 𝕖
return

<^<!(::
Send, 𝕗
return

<^<!=::
Send, 𝕘
return

<^<!*::
Send, 𝕙
return

<^<!)::
Send, 𝕚
return

<^<!+::
Send, 𝕛
return

<^<!]::
Send, 𝕜
return

<^<!!::
Send, 𝕝
return

<^<!#::
Send, 𝕞
return

<^<!;::
Send, 𝕟
return

<^<!,::
Send, 𝕠
return

<^<!.::
Send, 𝕡
return

<^<!p::
Send, 𝕢
return

<^<!y::
Send, 𝕣
return

<^<!f::
Send, 𝕤
return

<^<!g::
Send, 𝕥
return

<^<!c::
Send, 𝕦
return

<^<!r::
Send, 𝕧
return

<^<!l::
Send, 𝕨
return

<^<!/::
Send, 𝕩
return

<^<!@::
Send, 𝕪
return

<^<!a::
Send, 𝕫
return

<^<!+>$::
Send, 𝔸
return

<^<!+>&::
Send, 𝔹
return

<^<!+>[::
Send, ℂ
return

<^<!+>{::
Send, 𝔻
return

<^<!+>}::
Send, 𝔼
return

<^<!+>(::
Send, 𝔽
return

<^<!+>=::
Send, 𝔾
return

<^<!+>*::
Send, ℍ
return

<^<!+>)::
Send, 𝕀
return

<^<!+>+::
Send, 𝕁
return

<^<!+>]::
Send, 𝕂
return

<^<!+>!::
Send, 𝕃
return

<^<!+>#::
Send, 𝕄
return

<^<!+>;::
Send, ℕ
return

<^<!+>,::
Send, 𝕆
return

<^<!+>.::
Send, ℙ
return

<^<!+>p::
Send, ℚ
return

<^<!+>y::
Send, ℝ
return

<^<!+>f::
Send, 𝕊
return

<^<!+>g::
Send, 𝕋
return

<^<!+>c::
Send, 𝕌
return

<^<!+>r::
Send, 𝕍
return

<^<!+>l::
Send, 𝕎
return

<^<!+>/::
Send, 𝕏
return

<^<!+>@::
Send, 𝕐
return

<^<!+>a::
Send, ℤ
return

<^<^>!$::
Send, 𝓪
return

<^<^>!&::
Send, 𝓫
return

<^<^>![::
Send, 𝓬
return

<^<^>!{::
Send, 𝓭
return

<^<^>!}::
Send, 𝓮
return

<^<^>!(::
Send, 𝓯
return

<^<^>!=::
Send, 𝓰
return

<^<^>!*::
Send, 𝓱
return

<^<^>!)::
Send, 𝓲
return

<^<^>!+::
Send, 𝓳
return

<^<^>!]::
Send, 𝓴
return

<^<^>!!::
Send, 𝓵
return

<^<^>!#::
Send, 𝓶
return

<^<^>!;::
Send, 𝓷
return

<^<^>!,::
Send, 𝓸
return

<^<^>!.::
Send, 𝓹
return

<^<^>!p::
Send, 𝓺
return

<^<^>!y::
Send, 𝓻
return

<^<^>!f::
Send, 𝓼
return

<^<^>!g::
Send, 𝓽
return

<^<^>!c::
Send, 𝓾
return

<^<^>!r::
Send, 𝓿
return

<^<^>!l::
Send, 𝔀
return

<^<^>!/::
Send, 𝔁
return

<^<^>!@::
Send, 𝔂
return

<^<^>!a::
Send, 𝔃
return

<^<^>!+>$::
Send, 𝓐
return

<^<^>!+>&::
Send, 𝓑
return

<^<^>!+>[::
Send, 𝓒
return

<^<^>!+>{::
Send, 𝓓
return

<^<^>!+>}::
Send, 𝓔
return

<^<^>!+>(::
Send, 𝓕
return

<^<^>!+>=::
Send, 𝓖
return

<^<^>!+>*::
Send, 𝓗
return

<^<^>!+>)::
Send, 𝓘
return

<^<^>!+>+::
Send, 𝓙
return

<^<^>!+>]::
Send, 𝓚
return

<^<^>!+>!::
Send, 𝓛
return

<^<^>!+>#::
Send, 𝓜
return

<^<^>!+>;::
Send, 𝓝
return

<^<^>!+>,::
Send, 𝓞
return

<^<^>!+>.::
Send, 𝓟
return

<^<^>!+>p::
Send, 𝓠
return

<^<^>!+>y::
Send, 𝓡
return

<^<^>!+>f::
Send, 𝓢
return

<^<^>!+>g::
Send, 𝓣
return

<^<^>!+>c::
Send, 𝓤
return

<^<^>!+>r::
Send, 𝓥
return

<^<^>!+>l::
Send, 𝓦
return

<^<^>!+>/::
Send, 𝓧
return

<^<^>!+>@::
Send, 𝓨
return

<^<^>!+>a::
Send, 𝓩
return

<^d::
Send, https://discord.gg/dUPFfby
return

; Uncomment the below for screenshots if your computer has Snip And Sketch.

; ^\::
; Send, +#s
; return

; This is just for Apex Legends on my pc, lol.

; ^@::
; Run, "C:\Program Files (x86)\Steam\steamapps\common\Apex Legends\r5apex.exe"
; return